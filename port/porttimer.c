/*
 * FreeModbus Libary: MSP430 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: porttimer.c,v 1.3 2007/06/12 06:42:01 wolti Exp $
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Defines ------------------------------------------*/
/* Timer ticks are counted in multiples of 50us. Therefore 20000 ticks are
 * one second.
 */
#define MB_TIMER_TICKS          ( 20000L )

/* ----------------------- Static variables ---------------------------------*/
static BOOL bTimerInitialized = FALSE;

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit( USHORT usTim1Timeout50us )
{
	if( !bTimerInitialized ) {
		ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
		ROM_TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
		ROM_IntEnable(INT_TIMER1A);
		ROM_TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
		bTimerInitialized = TRUE;
	}

	ROM_TimerLoadSet(TIMER1_BASE, TIMER_BOTH, usTim1Timeout50us * (ROM_SysCtlClockGet()/MB_TIMER_TICKS));//TODO: check

    return TRUE;
}

void
vMBPortTimersEnable( void )
{
    /* Reset timer counter and set compare interrupt. */
	ROM_TimerDisable(TIMER1_BASE, TIMER_A);
	ROM_TimerEnable(TIMER1_BASE, TIMER_A);
}

void
vMBPortTimersDisable( void )
{
	ROM_TimerDisable(TIMER1_BASE, TIMER_A);
}

void
prvvMBTimerIRQHandler( void )
{
	ROM_TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    ( void )pxMBPortCBTimerExpired(  );
}
