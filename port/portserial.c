/*
 * FreeModbus Libary: MSP430 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.3 2006/11/19 03:57:49 wolti Exp $
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Defines ------------------------------------------*/
#define U0_CHAR                 ( 0x10 )        /* Data 0:7-bits / 1:8-bits */

#define DEBUG_PERFORMANCE       ( 0 )

#if DEBUG_PERFORMANCE == 1
#define DEBUG_PIN_RX            ( 0 )
#define DEBUG_PIN_TX            ( 1 )
#define DEBUG_PORT_DIR          ( P1DIR )
#define DEBUG_PORT_OUT          ( P1OUT )
#define DEBUG_INIT( )           \
  do \
  { \
    DEBUG_PORT_DIR |= ( 1 << DEBUG_PIN_RX ) | ( 1 << DEBUG_PIN_TX ); \
    DEBUG_PORT_OUT &= ~( ( 1 << DEBUG_PIN_RX ) | ( 1 << DEBUG_PIN_TX ) ); \
  } while( 0 ); 
#define DEBUG_TOGGLE_RX( ) DEBUG_PORT_OUT ^= ( 1 << DEBUG_PIN_RX )
#define DEBUG_TOGGLE_TX( ) DEBUG_PORT_OUT ^= ( 1 << DEBUG_PIN_TX )

#else

#define DEBUG_INIT( )
#define DEBUG_TOGGLE_RX( )
#define DEBUG_TOGGLE_TX( )
#endif

/* ----------------------- Static variables ---------------------------------*/
UCHAR           ucGIEWasEnabled = FALSE;
UCHAR           ucCriticalNesting = 0x00;

/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    ENTER_CRITICAL_SECTION(  );

    if( xRxEnable ) {
		ROM_UARTIntEnable(UART0_BASE, UART_INT_RX);
    } else {
		ROM_UARTIntDisable(UART0_BASE, UART_INT_RX);
    }

    if( xTxEnable ) {
		ROM_UARTIntEnable(UART0_BASE, UART_INT_TX);
		pxMBFrameCBTransmitterEmpty();
    } else {
		ROM_UARTIntDisable(UART0_BASE, UART_INT_TX);
    }

    EXIT_CRITICAL_SECTION(  );
}

BOOL
xMBPortSerialInit( UCHAR ucPort, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
	unsigned int uiUARTConf = 0;
	bool bInitialized = TRUE;

    ENTER_CRITICAL_SECTION(  );

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0|GPIO_PIN_1);

    EXIT_CRITICAL_SECTION(  );

    switch ( eParity )
    {
    case MB_PAR_NONE:
		uiUARTConf |= UART_CONFIG_PAR_NONE;
        break;
    case MB_PAR_ODD:
		uiUARTConf |= UART_CONFIG_PAR_ODD;
        break;
    case MB_PAR_EVEN:
		uiUARTConf |= UART_CONFIG_PAR_EVEN;
        break;
	default:
        bInitialized = FALSE;
        break;
    }

	switch ( ucDataBits )
	{
	case 8:
		uiUARTConf |= UART_CONFIG_WLEN_8;
        break;
	case 7:
		uiUARTConf |= UART_CONFIG_WLEN_7;
        break;
	case 6:
		uiUARTConf |= UART_CONFIG_WLEN_6;
        break;
	case 5:
		uiUARTConf |= UART_CONFIG_WLEN_5;
        break;
	default:
        bInitialized = FALSE;
        break;
	}

	uiUARTConf |= UART_CONFIG_STOP_ONE;

	if( bInitialized ) {
		ENTER_CRITICAL_SECTION();

		ROM_UARTConfigSetExpClk(UART0_BASE, ROM_SysCtlClockGet(), ulBaudRate, uiUARTConf);
		ROM_UARTFIFODisable(UART0_BASE);
		ROM_IntEnable(INT_UART0);

		EXIT_CRITICAL_SECTION();
	}

    return bInitialized;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
	ROM_UARTCharPut(UART0_BASE, ucByte);

	return TRUE;
}

BOOL
xMBPortSerialGetByte( CHAR * pucByte )
{
	do {
		*pucByte = ROM_UARTCharGet(UART0_BASE);
	} while(ROM_UARTCharsAvail(UART0_BASE));

	return TRUE;
}

void prvvMBSerialUARTIRQHandler( void )
{
	uint32_t status = ROM_UARTIntStatus(UART0_BASE, true);
	ROM_UARTIntClear(UART0_BASE, status);

	if(status & UART_INT_RX) {
		pxMBFrameCBByteReceived(  );
	}

	if(status & UART_INT_TX) {
		pxMBFrameCBTransmitterEmpty(  );
	}
}

void
EnterCriticalSection( void )
{
    if( ucCriticalNesting == 0 )
    {
        ucGIEWasEnabled = !ROM_IntMasterDisable();
    }
    ucCriticalNesting++;
}

void
ExitCriticalSection( void )
{
    ucCriticalNesting--;
    if( ucCriticalNesting == 0 )
    {
        if( ucGIEWasEnabled )
        {
            ROM_IntMasterEnable();
        }
    }
}
