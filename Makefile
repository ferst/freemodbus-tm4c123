CFLAGS += -Iport -Irtu -Iascii -Iinclude -Iport

SRC = port/portserial.c port/porttimer.c  \
      port/portevent.c \
      mb.c \
      rtu/mbrtu.c rtu/mbcrc.c \
      functions/mbfunccoils.c \
      functions/mbfuncdiag.c \
      functions/mbfuncholding.c \
      functions/mbfuncinput.c \
      functions/mbfuncother.c \
      functions/mbfuncdisc.c \
      functions/mbutils.c 

OBJS = $(SRC:.c=.o)

all: libfreemodbus.a

libfreemodbus.a: $(OBJS)
	$(AR) -rcs $@ $(OBJS)

clean:
	-$(RM) *.o */*.o
